package healthcheck

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type checker interface {
	check() error
}

type GinHandler struct {
	checkers []checker
}

func NewGinHandler(checkers ...checker) *GinHandler {
	return &GinHandler{
		checkers: checkers,
	}
}

func (h *GinHandler) Readiness(ctx *gin.Context) {
	for _, c := range h.checkers {
		if err := c.check(); err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
			return
		}
	}
	ctx.JSON(http.StatusOK, "success")
}
