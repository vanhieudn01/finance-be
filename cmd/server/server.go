package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"gitlab.com/vanhieudn01/finance-be/cmd/api"
	"gitlab.com/vanhieudn01/finance-be/pkg/middlewares"
	"gitlab.com/vanhieudn01/finance-be/pkg/services"
	"log"
	"net/http"
)

func Serve() {
	s := server{}
	s.init()
	s.start()
}

type server struct {
	redisClient *redis.Client
	router      *gin.Engine
	finService  services.FinService
	httpServer  *http.Server
}

func (s *server) init() {
	s.initRedis()
	s.initFinService()
	s.initRouter()
	s.initHttpServer()
}

func (s *server) initRedis() {

	option, err := redis.ParseURL(viper.GetString("redis.redis_url"))
	if err != nil {
		log.Fatal(err)
	}

	s.redisClient = redis.NewClient(option)
	if err := s.redisClient.Ping().Err(); err != nil {
		log.Fatal(err)
	}
}

func (s *server) initFinService() {
	s.finService = services.NewFinService()
}

func (s *server) initRouter() {
	r := gin.New()

	r.Use(middlewares.LoggingMiddleware(), gin.Recovery())

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/v1/financial-hello", api.NewFinHandler(s.finService).SendFinHello)
	r.GET("/ping", api.NewFinHandler(s.finService).Ping)

	s.router = r
}

func (s *server) initHttpServer() {
	s.httpServer = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", viper.GetString("setting.host"), viper.GetInt("setting.port")),
		Handler: s.router,
	}
}

func (s *server) start() {
	if err := s.httpServer.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
