package exceptions

type Error interface {
	Code() ErrorCode
	HttpStatusCode() int
	error
}

type GenericError struct {
	ErrCode ErrorCode
	Msg     string
}

func (gErr GenericError) Code() ErrorCode {
	return gErr.ErrCode
}

func (gErr GenericError) Error() string {
	return gErr.Msg
}

func (gErr GenericError) HttpStatusCode() int {
	return int(gErr.ErrCode) / 1000
}
