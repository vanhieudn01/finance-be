package api

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/vanhieudn01/finance-be/pkg/models"
	"gitlab.com/vanhieudn01/finance-be/pkg/services"
	"io/ioutil"
	"net/http"
)

type FinHandler struct {
	baseHandler
	finService services.FinService
}

func NewFinHandler(finService services.FinService) *FinHandler {
	return &FinHandler{
		finService: finService,
	}
}

// SendFinHello handles send fin hello request
// @tags FINANCIAL
// @summary Send financial service hello
// @description Send financial service hello
// @router /fin-hello [post]
// @param payload body finRequest true "Payload Data"
// @accept json
// @produce json
// @success 200 {object} string "success"
// @failure 400 "Bad Request"
// @failure 500 "Internal Server Error"
func (s FinHandler) SendFinHello(ctx *gin.Context) {
	reqBody, err := ioutil.ReadAll(ctx.Request.Body)

	if err != nil {
		s.buildErrorResponse(ctx, err)
		return
	}

	req := new(finRequest)
	if err := json.Unmarshal(reqBody, req); err != nil {
		s.buildErrorResponse(ctx, err)
		return
	}

	if err = s.finService.SendFinHello(req.toFin()); err != nil {
		s.buildErrorResponse(ctx, err)
		return
	}

	s.buildSuccessResponse(ctx, http.StatusOK)
}

// Ping handles send ping request
// @tags FINANCIAL
// @summary Send Ping
// @description Send Ping
// @router /ping [get]
// @param payload body finRequest true "Payload Data"
// @accept json
// @produce json
// @success 200 {object} string "success"
func (s FinHandler) Ping(ctx *gin.Context) {
	s.buildSuccessResponse(ctx, http.StatusOK)
}

type finRequest struct {
	Message string `json:"message"`
}

func (s finRequest) toFin() models.FinValidate {
	return models.FinValidate{
		Message: s.Message,
	}
}
