package validate

import (
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
	"log"
)

const (
	validationError = "validator failed to error"
	message         = "message"
)

var v = validator.New()

func init() {
	if err := v.RegisterValidation(message, isValidMessage); err != nil {
		log.Fatal(nil, validationError, zap.String("Tag Name", message))
	}
}

func Validator() *validator.Validate {
	return v
}

func isValidMessage(f validator.FieldLevel) bool {
	v := f.Field().String()

	if v == "" {
		return false
	}

	return true
}
