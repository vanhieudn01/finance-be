package container

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
)

var container *dig.Container

func BuildContainer() *dig.Container {
	container = dig.New()
	{
		_ = container.Provide(gin.New)
	}

	return container
}

func GetContainer() *dig.Container {
	return container
}
