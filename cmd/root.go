package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/vanhieudn01/finance-be/cmd/server"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "finance-be",
	Short: "fin service gateway boiler plate code",
	Run: func(cmd *cobra.Command, args []string) {
		server.Serve()
	},
}

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "config.dev.toml", "config file (default config.dev.toml)")
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigFile("config.dev")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("using config file: ", viper.ConfigFileUsed())
	}
}
