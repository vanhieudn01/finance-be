package healthcheck

import "github.com/go-redis/redis"

type RedisChecker struct {
	redisClient *redis.Client
}

func NewRedisChecker(c *redis.Client) *RedisChecker {
	return &RedisChecker{
		redisClient: c,
	}
}

func (r RedisChecker) check() error {
	return r.redisClient.Ping().Err()
}
