package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type baseHandler struct{}

func (baseHandler) buildErrorResponse(ctx *gin.Context, err error) {
	stt := http.StatusInternalServerError

	resp := response{
		Code:    http.StatusInternalServerError,
		Message: err.Error(),
	}

	ctx.JSON(stt, resp)
}

func (baseHandler) buildSuccessResponse(ctx *gin.Context, status int) {
	ctx.JSON(status, response{
		Code:    status,
		Message: "success call from backend",
	})
}
