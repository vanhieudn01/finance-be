FROM golang:1.13.0-stretch AS build

#RUN eval $(ssh-agent -s) && \
#    mkdir ~/.ssh && \
#    ssh-keyscan github.com > ~/.ssh/id-rsa \
#    cat /opt/private/gitlab_runner.priv > ~/.ssh/id_rsa && \
#    chmod 400 ~/.ssh/id_rsa

#RUN go get -u github.com/swaggo/swag/cmd/swag
WORKDIR /app
COPY ./go.mod .
RUN go mod download
COPY . /app
#RUN swag init --dir ./cmd
RUN CGO_ENABLED=0 GOOS=linux go build -o finance-be .

FROM alpine:3.9
#RUN apk --no-cache add ca-certificates
WORKDIR /app/
COPY --from=build /app/finance-be .
ENTRYPOINT ["./finance-be"]