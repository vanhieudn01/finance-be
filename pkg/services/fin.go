package services

import (
	"gitlab.com/vanhieudn01/finance-be/pkg/exceptions"
	"gitlab.com/vanhieudn01/finance-be/pkg/models"
	"gitlab.com/vanhieudn01/finance-be/pkg/validate"
)

type FinService interface {
	SendFinHello(models.FinValidate) error
}

type FinSer struct{}

func NewFinService() *FinSer {
	return &FinSer{}
}

func (f *FinSer) SendFinHello(finValidate models.FinValidate) error {
	if err := f.validateFin(finValidate); err != nil {
		return err
	}

	return nil
}

func (f *FinSer) validateFin(finVal models.FinValidate) error {
	if err := validate.Validator().Struct(finVal); err != nil {
		return exceptions.GenericError{
			ErrCode: exceptions.InvalidCall,
			Msg:     err.Error(),
		}
	}

	return nil
}
