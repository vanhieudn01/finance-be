package exceptions

type ErrorCode int

const (
	InvalidCall ErrorCode = iota + 200000
)
