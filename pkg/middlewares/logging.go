package middlewares

import (
	"github.com/gin-gonic/gin"
)

const headerRequestID = "X-REQUEST-ID"

func LoggingMiddleware(skipPaths ...string) gin.HandlerFunc {
	skipMap := make(map[string]struct{}, len(skipPaths))

	for _, p := range skipPaths {
		skipMap[p] = struct{}{}
	}

	return func(ctx *gin.Context) {
		//start := time.Now()
		//path := ctx.Request.URL.Path
		//requestID := ctx.Request.Header.Get(headerRequestID)
		//
		//ctx.Next()
		//
		//if _, ok := skipMap[path]; !ok {
		//	end := time.Now()
		//	responseTime := end.Sub(start)
		//	status := ctx.Writer.Status()
		//
		//	log.Info(nil, "",
		//		zap.String("path", path),
		//		zap.String("request_id", requestID),
		//		zap.Int("status", status),
		//		zap.String("method", ctx.Request.Method),
		//		zap.String("ip", ctx.ClientIP()),
		//		zap.String("user-agent", ctx.Request.UserAgent()),
		//		zap.Time("time", end),
		//		zap.Duration("response_time", responseTime),
		//	)
		//}
	}
}
